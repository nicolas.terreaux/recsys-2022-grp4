import time

import pandas as pd
import numpy as np
import nltk
import gensim.downloader as api

from utils import clean_data

lemmatizer = nltk.stem.WordNetLemmatizer()


def get_embedding(description, word_vectors) -> np.ndarray:
    # Get most similar words of each word in description
    similar_words = []
    for word in description:
        try:
            most_similar = word_vectors.most_similar_cosmul(positive=word)
            similar_words.append(most_similar)
        except KeyError:
            # A word from the description was not found in the pretrained word_embedding of GloVe
            # do nothing, just skip this word
            pass

    # Get vector of each similar word and calculate mean
    mean_vector = []
    for desc_word in similar_words:
        # Get word_embedding and weight of every similar word for one word in the description
        word_embeddings = []
        weights = []
        for word in desc_word:
            word_embeddings.append(word_vectors[word[0]])
            weights.append(word[1])

        # Calculate weighted mean with all similar words for one word in the description
        # Multiply each vector with each weight, add all weighted vectors, then divide by the sum of all weights
        weighted_vectors = []
        for i in range(len(word_embeddings)):
            weighted_vectors.append(word_embeddings[i] * weights[i])
        mean_vector.append(np.sum(weighted_vectors, axis=0) / np.sum(weights))

    # Calculate mean vector of all words in the description
    book_embedding = np.mean(mean_vector, axis=0)

    return book_embedding


def precalculate_with_title():
    # General information
    columns_used = ['book_title']
    counter = 1

    # Read the data
    print("Reading data")
    data_frame = pd.read_csv('../DataSet.csv')
    data_frame = data_frame[:10]
    print("Data loaded")

    # Drop some unused / unhelpful stuff
    data_frame.drop_duplicates(inplace=True)
    data_frame.dropna(subset=columns_used, inplace=True)

    # get all titles
    all_titles = data_frame['book_title']

    # Clean the data
    print("Cleaning data")
    data_frame = clean_data(data_frame, columns_used)
    print("Data cleaned")

    # Load pre-trained word-vectors from GloVe / gensim-data
    print("Load GloVe")
    word_vectors = api.load("glove-wiki-gigaword-50")  # Each word with 50 features
    print("GloVe loaded")

    # Initialize DataFrame to append vectors
    book_embeddings = pd.DataFrame()

    # Calculate all embeddings for every book in the dataset from their description
    times = []  # for statistics
    titles = []  # to append titles to dataframe later
    for index, row in data_frame.iterrows():
        start = time.time()
        book_embedding = get_embedding(row[columns_used[0]], word_vectors)  # get vector of description
        book_embeddings = book_embeddings.append(pd.Series(book_embedding), ignore_index=True)  # append vector to df
        titles.append(row['book_title'])
        times.append(time.time() - start)
        print("Book " + str(counter) + " done")
        counter += 1
    print("\nAll books done")
    print("Average time per book: " + str(np.mean(times)))
    print("Total time: " + str(np.sum(times)))

    # Add titles to DataFrame
    book_embeddings.insert(0, "Title", all_titles, True)

    # Save DataFrame to csv
    book_embeddings.to_csv('test_title.csv', index=False, header=False)


def precalculate_with_description():
    # General information
    columns_used = ['book_desc']
    counter = 1

    # Read the data
    print("Reading data")
    data_frame = pd.read_csv('../DataSet.csv')
    data_frame = data_frame[:10]
    print("Data loaded")

    # Drop some unused / unhelpful stuff
    data_frame.drop_duplicates(inplace=True)
    data_frame.dropna(subset=columns_used, inplace=True)

    # Clean the data
    print("Cleaning data")
    data_frame = clean_data(data_frame, columns_used)
    print("Data cleaned")

    # Load pre-trained word-vectors from GloVe / gensim-data
    print("Load GloVe")
    word_vectors = api.load("glove-wiki-gigaword-50")  # Each word with 50 features
    print("GloVe loaded")

    # Initialize DataFrame to append vectors
    book_embeddings = pd.DataFrame()

    # Calculate all embeddings for every book in the dataset from their description
    times = []  # for statistics
    titles = []  # to append titles to dataframe later
    for index, row in data_frame.iterrows():
        start = time.time()
        book_embedding = get_embedding(row[columns_used[0]], word_vectors)  # get vector of description
        book_embeddings = book_embeddings.append(pd.Series(book_embedding), ignore_index=True)  # append vector to df
        titles.append(row['book_title'])
        times.append(time.time() - start)
        print("Book " + str(counter) + " done")
        counter += 1
    print("\nAll books done")
    print("Average time per book: " + str(np.mean(times)))
    print("Total time: " + str(np.sum(times)))

    # Add titles to DataFrame
    book_embeddings.insert(0, "Title", titles, True)

    # Save DataFrame to csv
    book_embeddings.to_csv('test_desc.csv', index=False, header=False)


def main():
    # toggle the comment to use the function you want
    # we added the title function later, so we have some differences in the code, that's why we have two functions
    # precalculate_with_title()
    precalculate_with_description()


if __name__ == '__main__':
    main()
