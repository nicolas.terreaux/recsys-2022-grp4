import pandas as pd
import nltk

lemmatizer = nltk.stem.WordNetLemmatizer()


# Clean the data and tokenize the data
def clean_data(data_frame: pd.DataFrame, columns: list) -> pd.DataFrame:
    for column in columns:
        data_frame[column] = clean_column(data_frame[column])

    return data_frame


def drop_duplicates(frame: pd.DataFrame):
    frame.drop_duplicates(inplace=True)


# Remove the rows with NaN values
def remove_nan(frame: pd.DataFrame, column: str) -> pd.DataFrame:
    frame.dropna(subset=column, inplace=True)


# Clean the column
def clean_column(column: pd.Series) -> pd.Series:
    cleaned_column = column.apply(make_lower_case)  # Convert to lower case
    cleaned_column = cleaned_column.apply(tokenize)  # Tokenize
    cleaned_column = cleaned_column.apply(remove_special_characters)  # Remove special characters
    cleaned_column = cleaned_column.apply(lemmatize)  # Lemmatize
    cleaned_column = cleaned_column.apply(remove_stop_words)  # Remove stop words
    # cleaned_column = cleaned_column.apply(reconstruct_string)  # Reconstruct string

    return cleaned_column


def make_lower_case(text: str) -> str:
    return text.lower()


# Tokenize the data
def tokenize(text: str) -> list:
    return nltk.word_tokenize(text)


def remove_special_characters(words: list) -> list:
    return [word for word in words if word.isalpha()]


def lemmatize(words: list) -> list:
    return [lemmatizer.lemmatize(word) for word in words]


def remove_stop_words(words: list) -> list:
    stop_words = set(nltk.corpus.stopwords.words('english'))
    text = [word for word in words if not word in stop_words]
    return text


def reconstruct_string(words: list) -> str:
    return ' '.join(words)
