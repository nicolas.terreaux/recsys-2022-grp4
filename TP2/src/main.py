import pandas as pd
from sklearn.metrics.pairwise import cosine_similarity
import gensim.downloader as api

from utils import clean_data, tokenize, make_lower_case
from calcBookEmbedding import get_embedding

# ntltk downloads
# nltk.download('punkt')
# nltk.download('stopwords')
# nltk.download('wordnet')
# nltk.download('omw')
# nltk.download('omw-1.4')


# Main
if __name__ == '__main__':
    print("\n-- To use the titles and not descriptions, change the code described in the comments on line 37 and 74 --\n")

    print("--- Starting ---\n")

    # Change the configuration here
    # --------------------------
    number_of_recommendations = 5
    min_percent_acceptable = 60
    columns_used = ['description']
    clean = True

    # --------------------------
    # import original dataset
    print("Importing original dataset")
    dataFrame = pd.read_csv('../DataSet.csv')

    # Book to search with title and description
    title = "The Pacific"

    # If used with title change this line to:
    # description = dataFrame.loc[dataFrame['book_title'] == title, 'book_title'].iloc[0]
    description = dataFrame.loc[dataFrame['book_title'] == title, 'book_desc'].iloc[0]

    book = pd.DataFrame()

    # Choose a book not in the dataset, else it will give itself as a recommendation with 100% similarity
    book_name = [title]
    book_desc = [description]

    book['title'] = book_name
    book['description'] = book_desc

    # Clean the data to get the array of words
    if clean:
        print("Cleaning data")
        book = clean_data(book, columns_used)
    else:   # Option not to clean the data
        book['description'] = book['description'].apply(make_lower_case)    # doesn't work without lower case
        tokenized = tokenize(book['description'].iloc[0])   # get_embedding needs a list of words
        book['description'] = book['description'].apply(lambda x: tokenized)

    # Get the cleaned data
    descriptions = book[columns_used[0]]
    description = descriptions[0]

    # Load the precalculated embeddings
    print("Loading GloVe")
    word_vectors = api.load("glove-wiki-gigaword-50")

    # Calculate the embedding for the book
    print("Calculating embedding for book")
    book_embedding = get_embedding(description, word_vectors)
    book_embedding = book_embedding.reshape(1, -1)

    # Load the precalculated embeddings of our dataset and drop first column
    print("Loading precalculated embeddings")
    # If used with title or fewer entries change the path to one of the other files available
    embeddings = pd.read_csv('../word_vectors/book_embeddings_description_52000.csv', header=None)
    embeddings = embeddings.dropna().reset_index(drop=True)
    titles = embeddings[0]
    embeddings = embeddings.drop(embeddings.columns[0], axis=1)

    # Calculate the cosine similarity between the book and the dataset
    print("Calculating cosine similarity")
    similarity = cosine_similarity(book_embedding, embeddings)
    similarity_scores = pd.Series(similarity[0]).sort_values(ascending=False)
    indexes = similarity_scores.index

    # Get the top 5 recommendations
    print(f"\nTop {number_of_recommendations} recommendations for '{book_name[0]}':")

    # Get the indices of the top 5 most similar books
    top_5_indices = list(similarity_scores.iloc[1:number_of_recommendations+1].index)
    for index in top_5_indices:
        print(f"{titles.iloc[index]}: {round(similarity_scores[index] * 100, 2)}%")

    print("\n--- End of program ---")
