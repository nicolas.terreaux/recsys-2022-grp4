# TP2 doc

Nous avons choisi d'utilisé le modèle glove-wiki-gigaword-50 plutôt que le modèle glove-wiki-gigaword-100 car premièrement, nous obtenions des meilleures résultats et de plus, il était plus rapide à charger.

La différence entre les deux modèles est que le modèle glove-wiki-gigaword-100 contient 100 features alors que le modèle glove-wiki-gigaword-50 contient 50 features.

La description était pour nous la meilleure feature à utiliser pour GloVe car elle permet de mieux comprendre le sens des mots et donc de mieux comprendre le sens des phrases.

Nous avons tout de même essayé d'utiliser le titre. Nous obtenons des résultats autour des 95%, cependant, 
les livres proposés n'ont pas grand-chose à voir avec le livre recherché. Par exemple le livre "The Pacific" qui est un livre
sur la guerre mondiale, nous propose des livres sur les îles du pacifique comme nous pouvons le voir dans le résultat ci-dessous :

````
    Top 5 recommendations for 'The Pacific':
    The Islands at the End of the World: 95.73%
    The Island of the World: 95.17%
    Return to the Island: 95.08%
    Island in the Sea of Time: 95.0%
    Three Wells of the Sea: 94.72%
````

L'embedding des descriptions a été sauvegardée dans un fichier CSV afin de gagner du temps lors du calcul des top N.

Nous avons commencé par faire pour 10000 descriptions, puis 25000 et pour finir sur la totalité des descriptions (environ 52000).

A titre indicatif, le calcul de l'embedding des 52000 descriptions a pris environ 5h30 sur un PC fixe extrêmement puissant.

# Separation of concerns
Toutes les méthodes liées au nettoyage des données sont dans le fichier `utils.py`.

Nous avons fait un fichier calcBookEmbedding.py qui permet de calculer l'embedding des descriptions et de les sauvegarder dans un fichier CSV.

Nous avons pour finir un fichier main.py qui permet de lancer le programme.


# Tests
Pour les tests, nous avons repris les mêmes livres que pour le TP1. Les résultats sont beaucoup mieux que lors du TP1.
En effet, lorsque nous recherchons les livres sur Internet, nous voyons très clairement que les résultats sont corrects.

# Problème ouvert
Il est assez rare que nous obtenons cette erreur, mais de temps en temps, les résultats que nous obtenons sont extrêmement bizarres.
Parfois, il nous propose le même livre que celui que nous recherchons, comme dans l'exemple suivant :

````
    Top 5 recommendations for 'Harry Potter and the Chamber of Secrets':
    Harry Potter and the Chamber of Secrets: 99.95%
    Harry Potter and the Chamber of Secrets: 99.58%
    Harry Potter and the Prisoner of Azkaban: 98.39%
    Harry Potter and the Chamber of Secrets: 98.18%
    Harry Potter and the Chamber of Secrets: 98.07%
````

Nous n'avons aucune explications à ce sujet et nous n'avons pas réussi à le résoudre. Nous avons rencontré ce problème avec 2 livres sur
ceux que nous avons testés, mais il se peut que cela arrive avec d'autres livres.

### Solution
Apparemment, certains livres sont présents plusieurs fois dans l'ensemble de données, mais avec un type différent, ils n'ont donc pas été reconnus comme des doublons et n'ont donc pas été supprimés.

Par conséquent, notre méthode de nettoyage des données n'est pas assez bonne et c'est la raison de ce problème.

# Conclusion
Nous avons obtenu des résultats extrêmement satisfaisants avec une similitude qui est très proche de 1 pour chaque livre.
Même lorsque nous ne nettoyons pas les données, nous obtenons aussi d'excellents résultats.

Voici un exemple avec et sans nettoyage :
```
Cleaned:
Top 5 recommendations for 'The Pacific':
Dead Wake: The Last Crossing of the Lusitania: 98.18%
The Cardinal of the Kremlin: 98.12%
Once An Eagle: 98.12%
The Battle for Skandia: 98.12%
April 1865: The Month That Saved America: 98.11%


Not Cleaned:
Top 5 recommendations for 'The Pacific':
Apollyon: 98.47%
The Power of Six: 98.46%
Are You Afraid of the Dark?: 98.43%
102 Minutes: The Untold Story of the Fight to Survive Inside the Twin Towers: 98.43%
No Country for Old Men: 98.43%
```


Nous trouvons ça quand même assez surprenant, car nous n'avons pas l'habitude d'obtenir des résultats aussi satisfaisants.
