Présentation de glove: Bien.
- Tf/idf: comme dit en classe attention tf/idf ne calcule pas juste la fréquence d’un mot donc la “tf” (sinon parler plutôt du bag of words), mais il est bien vrai que c’est basé sur la fréquence d’apparition d’un mot plutôt que la co-occurence.
- Principe générale et comparaison avec tf/idf bien compris. Attention à l’exemple donné: dans cet exemple: chiens et chats n’apparaissent pas ensemble comme on croirait selon l’explication et le fonctionnement de glove, mais sont utilisés dans des phrases distinctes qui ont une structure similaire…donc exemples et explications pas tout à fait cohérents.

Présentation du code dans les slides:
- bien d’énumérer les étapes! Les choix sont clairs! (Notemment l’approche d’embedding par mot et groupe de mots ou champ texte en faisant la moyenne)
- des parties moins génériques de votre code aurait pu être partagés (notemment les lignes 26 à 43), et aussi de comment vous charger le modèle glove

Présentation des résultats:
Pas de mise en relief …


Mini rapport: même si je ne l’ai pas demandé, et que je ne peux pas me permettre de lire des rapports longs par TP, j’ai apprécié de lire le votre, qui était concis et clair et qui a bien résumé vos choix. Merci d’avoir pris le temps de le faire.


Code: 
- bravo pour l’approche de lancer les embeddings une seule fois et après les relire!

- au final, bravo pour votre approche d’embedding de chaque mot et après de la description à partir de celle des mots avec la moyenne à la ligne 43! (  book_embedding = np.mean(mean_vector, axis=0)). Cette ligne m’a manqué dans l’autre groupe qui travaille sur glove et ça change complètement l’approche et la performance.

Reporting des résultats dans code dans fichier excel:
- Aucune comparison, au niveau de la reproductibilité pas sûre qu’est ce que je dois passer comme commande pour reproduire les résultats (quel champ? Ou quel combinaison de champs?)
- Ok selon mes notes durant la présentation je comprend que c’est la description, il aurait été de le préciser dans l’excel. 
- C’est intéressant que la precision était toujours de 1…donc des 5 livres recommandés pour chacun des 20 livres, tous étaient jugés pertinents?




