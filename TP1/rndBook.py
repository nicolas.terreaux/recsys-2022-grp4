import pandas as pd

# This program is used to get 20 random books from the dataset
if __name__ == '__main__':
    # Load the data
    dataFrame = pd.read_csv('DataSet.csv')

    # Get all titles
    book_titles = dataFrame['book_title']

    book_titles = book_titles[:25000]

    # Get 20 random books from the dataset
    random_books = book_titles.sample(n=20)

    print(random_books)

    # write the random books to a csv file
    random_books.to_csv('random_books_fixed.csv', index=False)
