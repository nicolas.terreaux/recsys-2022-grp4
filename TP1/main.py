import re

import numpy as np
import pandas as pd
import nltk as nltk
import sklearn as sk
import matplotlib.pyplot as plt
import stopwords as stopwords
from nltk import RegexpTokenizer
from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.metrics.pairwise import cosine_similarity

# ntltk downloads
# nltk.download('punkt')
# nltk.download('stopwords')
# nltk.download('wordnet')
# nltk.download('omw')
# nltk.download('omw-1.4')

lemmatizer = nltk.stem.WordNetLemmatizer()


# Clean the data and tokenize the data
def clean_data(data_frame: pd.DataFrame, columns: list) -> pd.DataFrame:
    for column in columns:

        data_frame[column] = clean_column(data_frame[column])

    return data_frame


def drop_duplicates(frame: pd.DataFrame):
    frame.drop_duplicates(inplace=True)


# Remove the rows with NaN values
def remove_nan(frame: pd.DataFrame, column: str) -> pd.DataFrame:
    frame.dropna(subset=column, inplace=True)


# Clean the column
def clean_column(column: pd.Series) -> pd.Series:
    cleaned_column = column.apply(make_lower_case)  # Convert to lower case
    cleaned_column = cleaned_column.apply(tokenize)  # Tokenize
    cleaned_column = cleaned_column.apply(remove_special_characters)  # Remove special characters
    cleaned_column = cleaned_column.apply(lemmatize)  # Lemmatize
    cleaned_column = cleaned_column.apply(remove_stop_words)  # Remove stop words
    cleaned_column = cleaned_column.apply(reconstruct_string)  # Reconstruct string

    return cleaned_column


def make_lower_case(text: str) -> str:
    return text.lower()


# Tokenize the data
def tokenize(text: str) -> list:
    return nltk.word_tokenize(text)


def remove_special_characters(words: list) -> list:
    return [word for word in words if word.isalpha()]


def lemmatize(words: list) -> list:
    return [lemmatizer.lemmatize(word) for word in words]


def remove_stop_words(words: list) -> list:
    stop_words = set(nltk.corpus.stopwords.words('english'))
    text = [word for word in words if not word in stop_words]
    return text


def reconstruct_string(words: list) -> str:
    return ' '.join(words)


def remove_html(text: str):
    html_pattern = re.compile('<.*?>')
    return html_pattern.sub(r'', text)


def remove_punctuation(text: str):
    tokenizer = RegexpTokenizer(r'\w+')
    text = tokenizer.tokenize(text)
    text = " ".join(text)
    return text


def tf_idf(descriptions: pd.DataFrame, max_f=100) -> pd.DataFrame:
    print("Calculating TF/IDF")
    if max_f == -1:
        return TfidfVectorizer().fit_transform(descriptions)
    vectorizer = TfidfVectorizer(max_features=max_f, analyzer='char')
    return vectorizer.fit_transform(descriptions)


def compute_similarities(tfidf: pd.DataFrame, linear_kernel=False) -> pd.DataFrame:
    print("Calculating cosine similarities")
    if linear_kernel:
        return sk.metrics.pairwise.linear_kernel(tfidf, tfidf)
    return cosine_similarity(tfidf, tfidf)


def top_n(similarity_matrix, title: str, titles: pd.Series, depth=5):
    # Get the index of the book
    book_index = titles[titles == title].index[0]

    # Calcultate the cosine similarity mean for each cell in the matrix
    np.mean(similarity_matrix, axis=0)

    print(similarity_matrix[0])

    # Get the similarity scores
    similarity_scores = pd.Series(similarity_matrix[0][book_index]).sort_values(ascending=False)

    # Get the indices of the top 5 most similar books
    top_5_indices = list(similarity_scores.iloc[1:depth + 1].index)

    print("")
    print("Top 5 similar books to " + title + " are:")
    print("")

    sim_in_percent_rounded = []
    count_index = 0
    # Print the 5 titles most similar to the book with the similarity score
    for i in top_5_indices:
        print(titles.iloc[i])
        sim_in_percent_rounded.append(round(similarity_scores[i] * 100, 6))
        print(str(sim_in_percent_rounded[count_index]) + "%")
        count_index += 1
        print("---")

    # Return the top 5 most similar books
    return sim_in_percent_rounded


# Calculate the precision at n for the top n recommendations
def precision_at_n(min_percentage: float, top_n: pd.Series, depth: int):
    counter = 0
    for i in range(len(top_n)):
        if top_n[i] >= min_percentage:
            counter += 1
    return counter / depth


# Main
if __name__ == '__main__':
    # Change the configuration here
    # --------------------------
    book_name = 'Isabella Moon'
    columns_used = ['book_desc', 'genres']
    number_of_recommendations = 5
    min_percent_acceptable = 60
    clean = True
    # max features for the tfidf. If set to -1, all features will be used
    max_features_vectorizer = 100
    # If linear_kernel is set to True, the cosine similarity is calculated with linear_kernel
    linear_kernel = False


    # ----------------------------
    # Read the data
    print("Reading data")
    dataFrame = pd.read_csv('DataSet.csv')
    dataFrame = dataFrame[:25000]
    print("Data loaded")

    dataFrame.drop_duplicates(inplace=True)
    dataFrame.dropna(subset=columns_used, inplace=True)
    # Get all titles
    book_names = dataFrame['book_title']

    if clean:
        # Clean the data
        print("Cleaning data")
        dataFrame = clean_data(dataFrame, columns_used)
        print(dataFrame)
        print("Data cleaned")

    tfidf = {}
    for col in columns_used:
        tfidf[col] = tf_idf(dataFrame[col], max_features_vectorizer)

    print(tfidf)

    similarities = {}
    for col in columns_used:
        similarities[col] = compute_similarities(tfidf[col], linear_kernel)

    print(similarities)

    # get the top n most similar books
    top_n = top_n(list(similarities.values()), book_name, book_names, depth=number_of_recommendations)

    # Calculate the precision at n
    precision = precision_at_n(min_percent_acceptable, top_n, depth=number_of_recommendations)

    print("Precision at " + str(number_of_recommendations) + " is " + str(precision))
