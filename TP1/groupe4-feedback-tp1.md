# Feedback TP

- best representation of results so far, matches expectations 
- missing a README.md - I typically had to install a lib, I didn't have (stopwords) 
    I use this one....from nltk.corpus import stopwords
- great and simple way to build the testset. 
    - except that if I want to reproduce the results with your test set, it won't work, as it regenerates one at every run...it should have been saved and reused as required.
    - so you saved it here "random_books_fixed.csv" but didn't push it apparently...

- your threshold for cos_sim makes sense to avoid recommending items that are not similar enough to the input book, nevertheless it cannot be used as a evaluation metric ..
    - first it assumes if tf/idf is vectorizing in a non-specific way and yields two similar vector for two not so similar books, we cannot detect it without a ground truth or a manuak expert evaluation when labelled data is missing
    - second it is precise enough or more either as anything beyond 0.6 is accepted so a 0.8 and a 0.9 are considered equivalent...which is not consistent with your chosen metric, in which case we should give consider a method better if it yields more similar books!...

- why can't we pass as arguments to the program or to a function call, what we want to parametrise? instead of having to type it/change it directly in the function...

- otherwise you have done a good job, in terms of separation of concerns...

- of the few groups, that tested diff max_features values! too bad no other parameters were tested.

- éviter d'imprimer les matrices /Data frames / vecteurs longs et illibles...